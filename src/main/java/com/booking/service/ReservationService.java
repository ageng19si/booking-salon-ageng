package com.booking.service;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Service;
import com.booking.models.Reservation;
import com.booking.repositories.PersonRepository;
import com.booking.repositories.ServiceRepository;
import com.booking.service.*;
import lombok.experimental.UtilityClass;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@UtilityClass
public class ReservationService {

    private static final List<Reservation> reservations = new ArrayList<>();
    private static final Scanner scanner = new Scanner(System.in);

    public static void createReservation() {
        System.out.println("\nFitur Membuat Reservasi");

        System.out.print("Masukkan Reservation ID: ");
        String reservationId = scanner.nextLine();

        Customer customer = selectCustomer();
        if (customer == null) {
            System.out.println("Customer yang dicari tidak tersedia. Pembuatan reservasi dibatalkan.");
            return;
        }

        Employee employee = selectEmployee();
        if (employee == null) {
            System.out.println("Employee yang dicari tidak tersedia. Pembuatan reservasi dibatalkan.");
            return;
        }

        List<Service> selectedServices = selectServices();
        if (selectedServices.isEmpty()) {
            System.out.println("Pembuatan reservasi dibatalkan karena tidak ada service yang dipilih.");
            return;
        }

        Reservation reservation = new Reservation(reservationId, customer, employee, selectedServices, "In Process");
        reservations.add(reservation);

        System.out.println("Reservasi berhasil dibuat!");
        System.out.println("Workstage: In Process");
        System.out.println("Total Biaya: Rp." + reservation.getReservationPrice());
    }

    private static Customer selectCustomer() {
        System.out.print("Masukkan Customer ID: ");
        String customerId = scanner.nextLine();
        Customer customer = findCustomerById(customerId);

        while (customer == null) {
            System.out.println("Customer yang dicari tidak tersedia.");
            System.out.print("Silakan input ulang Customer ID yang tersedia: ");
            customerId = scanner.nextLine();
            customer = findCustomerById(customerId);
        }

        return customer;
    }

    private static Employee selectEmployee() {
        System.out.print("Masukkan Employee ID: ");
        String employeeId = scanner.nextLine();
        Employee employee = findEmployeeById(employeeId);

        while (employee == null) {
            System.out.println("Employee yang dicari tidak tersedia.");
            System.out.print("Silakan input ulang Employee ID yang tersedia: ");
            employeeId = scanner.nextLine();
            employee = findEmployeeById(employeeId);
        }

        return employee;
    }

    private static List<Service> selectServices() {
        List<Service> selectedServices = new ArrayList<>();
        List<Service> allServices = ServiceRepository.getAllService();

        while (true) {
            System.out.println("ketik stop jika sudah memilih service yang diinginkan");
            System.out.print("Masukkan Service ID: ");
            String serviceId = scanner.nextLine();

            if (serviceId.equalsIgnoreCase("stop")) {
                break;
            }

            Service selectedService = findServiceById(serviceId, allServices);

            if (selectedService == null) {
                System.out.println("Service yang dicari tidak tersedia.");
            } else if (selectedServices.contains(selectedService)) {
                System.out.println("Service sudah dipilih sebelumnya.");
            } else {
                selectedServices.add(selectedService);
                System.out.println("Service berhasil ditambahkan.");
    
                // Tampilkan pesan bahwa reservasi berhasil dibuat jika semua layanan telah ditambahkan
                if (selectedServices.size() == allServices.size()) {
                    System.out.println("Semua layanan telah ditambahkan. Reservasi berhasil dibuat!");
                }
            }
        }

        return selectedServices;
    }

    private static Customer findCustomerById(String id) {
        return PersonRepository.getAllCustomers().stream()
                .filter(customer -> customer.getId().equalsIgnoreCase(id))
                .findFirst()
                .orElse(null);
    }

    private static Employee findEmployeeById(String id) {
        return PersonRepository.getAllEmployees().stream()
                .filter(employee -> employee.getId().equalsIgnoreCase(id))
                .findFirst()
                .orElse(null);
    }

    private static Service findServiceById(String id, List<Service> services) {
        return services.stream()
                .filter(service -> service.getServiceId().equalsIgnoreCase(id))
                .findFirst()
                .orElse(null);
    }


    private static boolean isServiceSelected(String serviceId, List<Service> selectedServices) {
        return selectedServices.stream()
                .anyMatch(service -> service.getServiceId().equalsIgnoreCase(serviceId));
    }

    public static List<Reservation> getAllReservations() {
        return new ArrayList<>(reservations);
    }

    // public static void makeReservation() {
    //     System.out.println("\nFitur Membuat Reservasi\n");

    //     List<Service> selectedServices = selectServices();

    //     Customer customer = selectCustomer();

    //     Employee employee = selectEmployee();

    //     Reservation newReservation = new Reservation();
    //     newReservation.setCustomer(customer);
    //     newReservation.setEmployee(employee);
    //     newReservation.setServices(selectedServices);

    //     System.out.println("Reservasi berhasil dibuat!");
    // }

    public static void finishOrCancelReservation() {
        System.out.println("\nFitur Finish/Cancel reservation\n");

        List<Reservation> unfinishedReservations = getUnfinishedReservations(); 
                                                                               

        if (unfinishedReservations.isEmpty()) {
            System.out.println("Tidak ada reservasi yang belum selesai atau dibatalkan.");
            return;
        }

        System.out.println("Daftar Reservasi yang belum selesai atau dibatalkan:");
        for (int i = 0; i < unfinishedReservations.size(); i++) {
            Reservation reservation = unfinishedReservations.get(i);
            System.out.println((i + 1) + ". " + reservation.toString()); 
        }

        System.out.print("Pilih nomor reservasi yang ingin diselesaikan atau dibatalkan: ");
        int choice = scanner.nextInt(); 

        if (choice >= 1 && choice <= unfinishedReservations.size()) {
            Reservation selectedReservation = unfinishedReservations.get(choice - 1);
            System.out.println("Reservasi yang dipilih: " + selectedReservation.toString()); // Tampilkan detail
                                                                                            
        } else {
            System.out.println("Pilihan tidak valid.");
        }
    }

    public static List<Reservation> getUnfinishedReservations() {
        List<Reservation> allReservations = getAllReservations(); 
        List<Reservation> unfinishedReservations = new ArrayList<>();
        for (Reservation reservation : allReservations) {
            if (!reservation.getWorkstage().equals("Finish") && !reservation.getWorkstage().equals("Canceled")) {
                unfinishedReservations.add(reservation);
            }
        }
        return unfinishedReservations;
    }
    public static List<Reservation> getRecentReservations() {
        return new ArrayList<>();
    }


}