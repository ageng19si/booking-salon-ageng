package com.booking.service;
import java.util.List;

import com.booking.repositories.*;
import com.booking.service.*;
import com.booking.models.*;


public class PrintService {

    // public static void showRecentReservations(List<Reservation> reservations) {
    //     System.out.println("\nTampilan Recent Reservation\n");
    //     System.out.println("No\tID\tNama Customer\tService\tTotal Biaya\tWorkstage");
    //     int index = 1;
    //     for (Reservation reservation : reservations) {
    //         System.out.println(index++ + "\t" + reservation.getReservationId() + "\t" + reservation.getCustomer().getName() +
    //                 "\t" + formatServiceList(reservation.getServices()) + "\t" + reservation.getReservationPrice() +
    //                 "\t" + reservation.getWorkstage());
    //     }
    // }

    public static void showRecentReservations() {
        List<Reservation> recentReservations = ReservationService.getRecentReservations();
        if (recentReservations.isEmpty()) {
            System.out.println("Tidak ada reservasi terbaru.");
        } else {
            System.out.println("\nTampilan Recent Reservation\n");
            System.out.println("No\tID\tNama Customer\tService\tTotal Biaya\tWorkstage");
            int index = 1;
            for (Reservation reservation : recentReservations) {
                System.out.println(index++ + "\t" + reservation.getReservationId() + "\t" + reservation.getCustomer().getName() +
                        "\t" + formatServiceList(reservation.getServices()) + "\t" + reservation.getReservationPrice() +
                        "\t" + reservation.getWorkstage());
            }
        }
    }
    

    private static String formatServiceList(List<Service> services) {
        StringBuilder serviceList = new StringBuilder();
        for (Service service : services) {
            serviceList.append(service.getServiceName()).append(", ");
        }
        return serviceList.substring(0, serviceList.length() - 2);
    }


    public static void showCustomers() {
        List<Person> persons = PersonRepository.getAllPerson();
        System.out.println("\nTampilan Customer\n");
        System.out.println("No\tID\tNama\tAlamat\tMembership\tUang");
        int index = 1;
        for (Person person : persons) {
            if (person instanceof Customer) {
                Customer customer = (Customer) person;
                System.out.println(index++ + "\t" + customer.getId() + "\t" + customer.getName() +
                        "\t" + customer.getAddress() + "\t" + customer.getMember().getMembershipName() +
                        "\t" + customer.getWallet());
            }
        }
    }

    public static void showService(){
        List<Service> services = ServiceRepository.getAllService();
        System.out.println("\nTampilan Service\n");
        System.out.println("No\tID\tNama\t\t\tHarga");
        int index = 1;
        for (Service service : services){
            if (service instanceof Service){
               
                System.out.println(index++ + "\t" + service.getServiceId() + "\t" + service.getServiceName() +"\t\t" + service.getPrice());
            }
        }
    }

    public static void showEmployees() {
        List<Person> persons = PersonRepository.getAllPerson();
        System.out.println("\nTampilan Employee\n");
        System.out.println("No\tID\tNama\tAlamat\tPengalaman");
        int index = 1;
        for (Person person : persons) {
            if (person instanceof Employee) {
                Employee employee = (Employee) person;
                System.out.println(index++ + "\t" + employee.getId() + "\t" + employee.getName() +
                        "\t" + employee.getAddress() + "\t" + employee.getExperience());
            }
        }
    }

    public static void showReservationHistory(List<Reservation> reservations) {
        System.out.println("\nTampilan History Reservation\n");
        System.out.println("No\tID\tNama Customer\tService\tTotal Biaya\tWorkstage");
        int index = 1;
        for (Reservation reservation : reservations) {
            System.out.println(index++ + "\t" + reservation.getReservationId() + "\t" + reservation.getCustomer().getName() +
                    "\t" + formatServiceList(reservation.getServices()) + "\t" + reservation.getReservationPrice() +
                    "\t" + reservation.getWorkstage());
        }
        double totalProfit = calculateTotalProfit(reservations);
        System.out.println("Total Keuntungan\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tRp." + totalProfit);
    }

    private static double calculateTotalProfit(List<Reservation> reservations) {
        double totalProfit = 0;
        for (Reservation reservation : reservations) {
            if (reservation.getWorkstage().equals("Finish")) {
                totalProfit += reservation.getReservationPrice();
            }
        }
        return totalProfit;
    }
}