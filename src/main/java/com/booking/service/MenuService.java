package com.booking.service;

import java.util.List;
import java.util.Scanner;

import com.booking.models.Reservation;

public class MenuService {

    private static Scanner scanner = new Scanner(System.in);

    public static void mainMenu() {
        int choice;
        do {
            System.out.println("\nAplikasi Booking Salon\n");
            System.out.println("\t1. Tampilkan Data");
            System.out.println("\t2. Membuat Reservasi");
            System.out.println("\t3. Finish/Cancel Reservasi");
            System.out.println("\t0. Exit\n");
            System.out.print("Pilihan Anda: ");
            choice = scanner.nextInt();
            switch (choice) {
                case 1:
                    showDataMenu();
                    break;
                case 2:
                    ReservationService.createReservation();
                    break;
                case 3:
                    ReservationService.finishOrCancelReservation();
                    break;
                case 0:
                    System.out.println("Terima kasih telah menggunakan aplikasi Booking Salon.");
                    break;
                default:
                    System.out.println("Pilihan tidak valid. Silakan pilih kembali.");
            }
        } while (choice != 0);
    }

    private static void showDataMenu() {
        int choice;
        do {
            System.out.println("\nSub menu Tampilkan Data\n");
            System.out.println("\tData Booking Salon\n");
            System.out.println("\t1. Tampilkan Recent Reservation");
            System.out.println("\t2. Tampilkan Customer");
            System.out.println("\t3. Tampilkan Employee");
            System.out.println("\t4. Tampilkan History Reservation + Total Keuntungan");
            System.out.println("\t5. Tampilkan Service");
            System.out.println("\t0. Back To Main Menu\n");
            System.out.print("Pilihan Anda: ");
            choice = scanner.nextInt();
            switch (choice) {
                case 1:
                    PrintService.showRecentReservations();
                    break;
                case 2:
                    PrintService.showCustomers();
                    break;
                case 3:
                    PrintService.showEmployees();
                    break;
                case 4:
                    List<Reservation> allReservations = ReservationService.getAllReservations();
                    PrintService.showReservationHistory(allReservations);
                    break;
                case 5:
                    PrintService.showService();
                    break;
                case 0:
                    break;
                default:
                    System.out.println("Pilihan tidak valid. Silakan pilih kembali.");
            }
        } while (choice != 0);
    }
}