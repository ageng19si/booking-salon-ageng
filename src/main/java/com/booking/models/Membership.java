package com.booking.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Membership {
    private String memberId;
    private String membershipName;

    @Override
    public String toString() {
        return "Member ID: " + memberId + ", Membership Name: " + membershipName;
    }
}
