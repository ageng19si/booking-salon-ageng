package com.booking.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Customer extends Person {
    private double wallet;
    private Membership member;
    public double calculateDiscount(double totalPrice) {
        switch (member.getMembershipName()) {
            case "none":
                return 0;
            case "Silver":
                return totalPrice * 0.05; // 5% discount for Silver members
            case "Gold":
                return totalPrice * 0.10; // 10% discount for Gold members
            default:
                return 0; // No discount for other membership levels
        }
    }
}    
