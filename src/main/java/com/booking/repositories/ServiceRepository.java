package com.booking.repositories;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.booking.models.Service;

public class ServiceRepository {
    private static List<Service> serviceList = Arrays.asList(
        new Service("Serv-01", "Potong Rambut", 75000),
        new Service("Serv-02", "Styling Rambut", 125000),
        new Service("Serv-03", "Pewarnaan Rambut", 500000),
        new Service("Serv-04", "Rebonding", 60000),
        new Service("Serv-05", "Mengeriting Rambut", 70000)
    );

    public static List<Service> getAllService() {
        return new ArrayList<>(serviceList);
    }

    public static Service findServiceById(String id) {
        for (Service service : serviceList) {
            if (service.getServiceId().equals(id)) {
                return service;
            }
        }
        return null;
    }
}
